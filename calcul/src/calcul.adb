with Ada.Text_IO;
procedure Calcul is
   Chiffre1 : Integer :=5;
   Chiffre2 : Integer :=6;
   Somme : Integer;
begin
   Somme := Chiffre1 + Chiffre2;
   Ada.Text_IO.Put_Line("La somme de Chiffre1 et Chiffre2 est : " & Integer'Image(Somme));
end Calcul;
