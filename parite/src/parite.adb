with Ada.Text_IO;

procedure Parite is

   procedure CheckParite (Num : Integer) is
      Resultat : Integer;
   begin
      Resultat := Num rem 2;

      if Resultat = 0 then
         Ada.Text_IO.Put_Line("Le nombre " & Integer'Image(Num) & " est pair.");
      else
         Ada.Text_IO.Put_Line("Le nombre " & Integer'Image(Num) & " est impair.");
      end if;
   end CheckParite;

   -- Déclaration explicite du package Integer_IO
   package Integer_IO is new Ada.Text_IO.Integer_IO (Num => Integer);

begin
   -- Demander à l'utilisateur d'entrer un nombre
   Ada.Text_IO.Put("Entrez un nombre : ");
   declare
      Input : Integer;
   begin
      Integer_IO.Get(Item => Input); -- Utilisation de Integer_IO.Get

      -- Appeler la procédure CheckParite pour vérifier la parité
      CheckParite(Input);
   exception
      when others =>
         Ada.Text_IO.Put_Line("Erreur : Veuillez entrer un nombre entier valide.");
   end;
end Parite;
