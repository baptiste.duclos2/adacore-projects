-- Personnage_Pkg.adb
with Ada.Text_IO;

package body Personnage_Pkg is

   procedure Creer_Personnage(Un_Personnage : out Personnage; Nom : Nom_Personnage; Points_De_Vie : Integer) is
   begin
      -- Vérifier que la longueur de Nom ne dépasse pas la limite
      if Nom'Length > 99 then
         raise Program_Error with "Longueur de Nom dépasse la limite de 100 caractères";
      end if;

      -- Initialiser d'autres champs si nécessaire
      Un_Personnage.Nom := Nom;
      Un_Personnage.PV := Points_De_Vie;
   end Creer_Personnage;


   procedure Deplacer(Un_Personnage : in out Personnage; Nouvelle_Position : Position) is
   begin
      Un_Personnage.Pos := Nouvelle_Position;
   end Deplacer;

   procedure Information_Personnage(Un_Personnage : in Personnage) is
   begin
      Ada.Text_IO.Put_Line(Un_Personnage.Nom'Image & " est un personnage avec " & Integer'Image(Un_Personnage.PV) & " Points de vie");
   end Information_Personnage;

end Personnage_Pkg;


