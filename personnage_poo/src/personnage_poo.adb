with Personnage_Pkg;

procedure Personnage_Poo is
   Mon_Hero : Personnage_Pkg.Personnage;
begin
   -- Appel à la procédure Creer_Personnage avec tous les arguments requis
   Personnage_Pkg.Creer_Personnage(Un_Personnage => Mon_Hero, Nom => "Un", Points_De_Vie => 100);

   -- Utilisez Mon_Hero comme nécessaire
   Personnage_Pkg.Information_Personnage(Mon_Hero);
end Personnage_Poo;
