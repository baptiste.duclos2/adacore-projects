package Arme is
    type Arme is tagged private;

    function Creer_Arme(Nom : String; Degats_Arme : Integer) return Arme;

    private
    type Arme is tagged record
        Nom_Arme : String(1..100);
        Degats_Arme : Integer;
    end record;
end Arme;