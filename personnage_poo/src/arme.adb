package body Arme is
    function Creer_Arme(Nom: String; Degats_Arme : Integer) return Arme is
    begin
        Arme.Nom_Arme := Nom;
        Arme.Degats_Arme := Degats_Arme;
    end Creer_Arme;
end Arme;