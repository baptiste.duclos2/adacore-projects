package Position is 
    type Position is tagged private;

    function Creer_Position(Pos : String);

    private
    type Position is tagged record
        Nom_Position : String;
    end record;
end Position;