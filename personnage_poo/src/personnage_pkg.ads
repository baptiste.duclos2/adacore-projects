-- Personnage_Pkg.ads

with Ada.Text_IO;

package Personnage_Pkg is

   type Arme is (Epee, Baton, Arc);
   type Position is (Debout, Assis, Allonge);
   type Nom_Personnage is new String(1..100);
   type Personnage is tagged private;

   procedure Creer_Personnage(Un_Personnage : out Personnage; Nom : Nom_Personnage; Points_De_Vie : Integer);
   procedure Deplacer(Un_Personnage : in out Personnage; Nouvelle_Position : Position);
   procedure Information_Personnage(Un_Personnage : Personnage_Pkg.Personnage);

private

   type Personnage is tagged record
      Nom : Nom_Personnage;
      PV : Integer;
      Arme_Equipee : Arme;
      Pos : Position;
   end record;

end Personnage_Pkg;
