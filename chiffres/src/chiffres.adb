with Ada.Text_IO;

procedure Chiffres is
   Nombre1, Nombre2, Nombre3, Nombre4, Nombre5, Resultat : Integer;
   package Integer_IO is new Ada.Text_IO.Integer_IO (Num => Integer);
begin
   Ada.Text_IO.Put("Veuillez entrer un premier chiffre");
   Integer_IO.Get(Item => Nombre1);

   Ada.Text_IO.Put("Veuillez entrer un deuxieme chiffre");
   Integer_IO.Get(Item => Nombre2);

   Ada.Text_IO.Put("Veuillez entrer un troisieme chiffre");
   Integer_IO.Get(Item => Nombre3);

   Ada.Text_IO.Put("Veuillez entrer un quatrieme chiffre");
   Integer_IO.Get(Item => Nombre4);

   Ada.Text_IO.Put("Veuillez entrer un cinquieme chiffre");
   Integer_IO.Get(Item => Nombre5);

   Resultat := Nombre1 + Nombre2 + Nombre3 + Nombre4 + Nombre5;

   Ada.Text_IO.Put("Le résultat est egal à : " & Integer'Image(Resultat));
end Chiffres;
