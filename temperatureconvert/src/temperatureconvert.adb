with Ada.Text_IO;

procedure Temperatureconvert is
   Temperature, Resultat : Float;

   package Float_IO is new Ada.Text_IO.Float_IO (Num => Float);
   package Integer_IO is new Ada.Text_IO.Integer_IO (Num => Integer);

begin
   Ada.Text_IO.Put("Veuillez entrer la temperature : ");
   Float_IO.Get(Item => Temperature);

   Ada.Text_IO.Put("Quelle est l'unite de mesure de votre temperature ? ");
   Ada.Text_IO.Put("1 : Celsius ");
   Ada.Text_IO.Put("2 : Fahrenheit ");

   declare
      Choix : Integer;
   begin
      Ada.Text_IO.Put("Votre choix : ");
      Integer_IO.Get(Item => Choix);

      case Choix is
         when 1 =>
            Resultat := (9.0/5.0) * Temperature + 32.0;
            Ada.Text_IO.Put("Votre temperature est de : ");
            Float_IO.Put(Item => Temperature, Fore => 2, Aft => 2, Exp => 0);
            Ada.Text_IO.Put(" Celsius et : ");
            Float_IO.Put(Item => Resultat, Fore => 2, Aft => 2, Exp => 0);
            Ada.Text_IO.Put_Line(" Fahrenheit!");
         when 2 =>
            Resultat := (5.0/9.0) * (Temperature - 32.0);
            Ada.Text_IO.Put("Votre temperature est de : ");
            Float_IO.Put(Item => Temperature, Fore => 2, Aft => 2, Exp => 0);
            Ada.Text_IO.Put(" Fahrenheit et : ");
            Float_IO.Put(Item => Resultat, Fore => 2, Aft => 2, Exp => 0);
            Ada.Text_IO.Put_Line(" Celsius!");
         when others =>
            Ada.Text_IO.Put_Line("Veuillez ajouter un nombre valide!");
            return;
      end case;
   end;
end Temperatureconvert;