with Ada.Text_IO;

procedure Calculatrice is
   Nombre1, Nombre2, Resultat : Float;

   package Float_IO is new Ada.Text_IO.Float_IO (Num => Float);
   package Integer_IO is new Ada.Text_IO.Integer_IO (Num => Integer);
begin
   Ada.Text_IO.Put_Line("Veuillez entrer le premier nombre : ");
   Float_IO.Get(Item => Nombre1);

   Ada.Text_IO.Put_Line("Veuillez entrer le second nombre : ");
   Float_IO.Get(Item => Nombre2);

   -- Demander l'opération à effectuer
   Ada.Text_IO.Put_Line("Choisissez une opération :");
   Ada.Text_IO.Put_Line("1. Addition");
   Ada.Text_IO.Put_Line("2. Soustraction");
   Ada.Text_IO.Put_Line("3. Multiplication");
   Ada.Text_IO.Put_Line("4. Division");

   declare 
      Choix : Integer;
   begin
      Ada.Text_IO.Put_Line("Votre choix : ");
      Integer_IO.Get(Item => Choix);

      -- Effectuer l'opération
      case Choix is
         when 1 =>
            Resultat := Nombre1 + Nombre2;
         when 2 => 
            Resultat := Nombre1 - Nombre2;
         when 3 =>
            Resultat := Nombre1 * Nombre2;
         when 4 =>
            if Nombre2 /= 0.0 then
               Resultat := Nombre1 / Nombre2;
            else 
               Ada.Text_IO.Put_Line("Erreur : Division par zéro.");
               return;
            end if;
         when others =>
            Ada.Text_IO.Put_Line("Erreur : Choix invalide.");
            return;
      end case;

      -- Affichage du résultat
      Ada.Text_IO.Put_Line("Le résultat est : ");
      Float_IO.Put(Resultat, Fore => 2, Aft => 2);
   end;
end Calculatrice;
