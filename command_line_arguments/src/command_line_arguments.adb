with Ada.Text_IO;
with Ada.Command_Line;

procedure Calculate_Sum is
begin
   for Next in reverse 1 .. Ada.Command_Line.Argument_Count loop
      Ada.Text_IO.Put_Line(Ada.Command_Line.Argument (Next));
   end loop;
end Calculate_Sum;
